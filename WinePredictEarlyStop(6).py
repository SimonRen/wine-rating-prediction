from keras import regularizers
from keras.utils import to_categorical
from keras.optimizers import adam
from keras.models import Model
from keras.layers import Dense,Input,BatchNormalization,Dropout
from keras.constraints import max_norm
from keras.callbacks import EarlyStopping
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from random import shuffle
from sklearn import metrics
import pdb

# Read the data, split into training and testing sets and between inputs (x) and outputs (y)
dataset = pd.read_csv('winequality-red.csv',sep=';', header=1).values.astype('float32')
PropTest = 0.1
NTrain=int((1-PropTest)*dataset.shape[0])

dataset_train=dataset[:NTrain,:]
dataset_test=dataset[NTrain:,:]

shuffle(dataset_train) # random permutations

x_train= dataset_train[:,:-1] # input for training
y_train= dataset_train[:,-1] # output for training

x_test= dataset_test[:,:-1] # input for testing
y_test= dataset_test[:,-1] # output for testing

# Convert each output element from number to vector containing 0 and 1 => e.g. : 2 becomes [0,0,1,0,0,0,0,0,0,0,0]
y_train = to_categorical(y_train, 10)
y_test = to_categorical(y_test, 10)
# Keep only the class that exists in the data (3->9 /10)
LowestIndex=3
y_train=y_train[:,LowestIndex:]
y_test=y_test[:,LowestIndex:]

# Definition of the model
weight_decay=1e-4
InputTensor = Input(shape=(x_train.shape[1],))
Tensor = BatchNormalization()(InputTensor)
Tensor=Dense(80,activation='relu',kernel_regularizer=regularizers.l2(weight_decay))(Tensor) 
Tensor=Dense(80,activation='relu',kernel_regularizer=regularizers.l2(weight_decay))(Tensor)
Tensor=Dropout(0.1)(Tensor)
OutTensor=Dense(y_train.shape[1],activation='softmax',kernel_regularizer=regularizers.l2(weight_decay))(Tensor)

model=Model(InputTensor,OutTensor)

# Compile the model: Define the optimizer and the loss function
LearningRate= 1e-4
Optimizer=adam(lr=LearningRate)
model.compile(loss='categorical_crossentropy', optimizer=Optimizer,metrics=['accuracy'])

# Train the model
PropVal=0.1
batch_size = 32
epochs = 1000
callbacks=[EarlyStopping(monitor='val_loss', patience=40, verbose=2, mode='min')]
history=model.fit(x_train, y_train, validation_split=PropVal,epochs=epochs, batch_size=batch_size,callbacks=callbacks,verbose=2)

# Plot the evolution of accuracy during the training
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy with early stopping')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

# Plot the evolution of loss during the training
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss with early stopping')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

# Test the model
print(model.evaluate(x_test,y_test,verbose=1))
ResultInference=model.predict(x_test)
NumTest=np.argmax(y_test,axis=1)
NumPred=np.argmax(ResultInference,axis=1)

NShow=37
print('Inference: ',NumPred[:NShow]+3)
print('y data:    ',NumTest[:NShow]+3)

print(metrics.confusion_matrix(NumTest,NumPred))
print(metrics.precision_score(NumTest, NumPred, average=None))

MeanError=np.mean(np.abs(NumTest-NumPred))
print('Mean Error: ',MeanError)